﻿:- (dynamic bloque/2).
:- (dynamic bloqueLibre/2).
:- (dynamic nodo/3).
:- (dynamic caminor/2).

% agregarBloque(+BloX, +BloY).
% Agrega un bloque sólido (no navegable) a la base de datos si este no existe.
agregarBloque(BloX, BloY):-
    (
        bloque(BloX, BloY), !
    ;   assertz(bloque(BloX, BloY))
    ).
 
% agregarBloqueLibre(+BloX, +BloY).
% Agrega un bloque libre (navegable) a la base de datos si este no existe.
agregarBloqueLibre(BloX, BloY):-
    (
        bloqueLibre(BloX, BloY), !
    ;   assertz(bloqueLibre(BloX, BloY))
    ).
 
% agregarAdyacentes(+Blo1, +Blo2).
% Agrega dos bloques adyacentes a la base de datos si estos no existen.
agregarAdyacentes(Blo1, Blo2):-
    (
        nodo(Blo1, Blo2, 1), !
    ;   nodo(Blo2, Blo1, 1), !
    ;   assertz(nodo(Blo1, Blo2, 1))
    ).
 
% generarAdyacentes.
% Agrega a la base de datos todos los bloques libres que son adyacentes.
generarAdyacentes:-
    findall([[X, Y], [W, Z]], (
        (
            bloqueLibre(X, Y), bloqueLibre(W, Z), W is X + 1, Z = Y
        ;   bloqueLibre(X, Y), bloqueLibre(W, Z), W is X - 1, Z = Y
        ;   bloqueLibre(X, Y), bloqueLibre(W, Z), W = X, Z is Y + 1
        ;   bloqueLibre(X, Y), bloqueLibre(W, Z), W = X, Z is Y - 1
        )
    ), Adyacentes),
    foreach(member([Blo1, Blo2], Adyacentes), agregarAdyacentes(Blo1, Blo2)).
 
% camino(+Desde, +Hacia, +Distancia)
% Existe un camino si hay un nodo que une A y B.
camino(A, B, Distancia) :- nodo(A, B, Distancia).
camino(A, B, Distancia) :- nodo(B, A, Distancia).
 
%%%%%%%%%%%%%%%%%%%%%%%%%%% Comportamiendo Pynky %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
% perseguirPinky(+PacX, +PacY, +FanX, +FanY, -Accion).
% Indica que acción realizar para Pinky.
% Debe perseguir a pacman cuando se encuentran en el mismo eje X o Y.
perseguirPinky(PacX, PacY, FanX, FanY, Accion):-
    fantasmaVeAPacman(PacX, PacY, FanX, FanY),
    calcularAccion(PacX, PacY, FanX, FanY, Accion),
    !.
perseguirPinky(_, _, _, _, nada).
 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%% Comportamiendo Inky %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
% perseguirInky(+PacX, +PacY, +FanX, +FanY, -Accion).
% Indica que acción realizar para Inky.
% Debe perseguir a pacman cuando se encuentran en el mismo eje X o Y y la distancia entre ellos es de a
% lo sumo 5 bloques.
perseguirInky(PacX, PacY, FanX, FanY, Accion):-
    fantasmaVeAPacman(PacX, PacY, FanX, FanY),
    calcularDistancia(PacX, PacY, FanX, FanY, Distancia),
    Distancia =< 5,
    calcularAccion(PacX, PacY, FanX, FanY, Accion),
    !.
perseguirInky(_, _, _, _, nada). 

% fantasmaVeAPacman(+PacX, +PacY, +FanX, +FanY).
% Determina si pacman y un fantasma se encuentran en el mismo eje X o Y.
fantasmaVeAPacman(PacX, PacY, FanX, FanY):-
    PacX = FanX,
    forall(bloque(PacX, BloY),
    not(interfiereVision(BloY,PacY,FanY))), !.
 
fantasmaVeAPacman(PacX, PacY, FanX, FanY):-
    PacY = FanY,
    forall(bloque(BloX, PacY),
    not(interfiereVision(BloX,PacX,FanX))), !.
 
fantasmaVeAPacman(_, _, _, _):- fail.

% interfiereVision(Bloq+,Pac+,Fan+).
% Determina si el bloque solido Bloq se encuentra en el camino entre pacman
% Pac y el fantasma Fan.
% TODO: Definir el predicado completo.
interfiereVision(Bloq,Pac,Fan).
 
% calcularAccion(+PacX, +PacY, +FanX, +FanY, -Accion).
% Calcula la acción a realizar por el fantasma de acuerdo a su posición respecto a pacman.
% TODO: Definir el predicado completo.
% ACLARACIÓN: de tener éxito el quinto argumento debería unificar con arriba, derecha, abajo
% o izquierda, en caso contrario con nada.
calcularAccion(PacX, PacY, FanX, FanY, nada).
 
% calcularDistancia(+PacX, +PacY, +FanX, +FanY, -DistanciaAbs).
% Calcula la distancia absoluta entre dos puntos (X, Y).
% En este caso entre pacman y un fantasma.
% TODO: Definir el predicado completo.
calcularDistancia(PacX, PacY, FanX, FanY, 0).
 
%%%%%%%%%%%%%%%%%%%%%%%%%%% Comportamiendo Clyde %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
% perseguirClyde(+FanX, +FanY, -Accion).
% Indica que acción realizar para Clyde.
% Persigue a pacman eligiendo un bloque aleatorio. Si existiese un bloque sólido, se genera una nueva
% posición hasta conseguir una libre.
perseguirClyde(FanX, FanY, Accion):-
    findall(X, bloque(X, _), Xs),
    findall(Y, bloque(_, Y), Ys),
    max_list(Xs, MaxX),
    max_list(Ys, MaxY),
    generarAleatorio(0, MaxX, 0, MaxY, AleX, AleY),
    (
        (AleX = FanX; AleY = FanY),
        calcularAccion(AleX, AleY, FanX, FanY, Accion),
        !
    ;   calcularAccion2(AleX, AleY, FanX, FanY, Acciones),
        random_member(Accion, Acciones)
    ).
perseguirClyde(_, _, _, _, nada).
 
% generarAleatorio(+MinValorX, +MaxValorX, +MinValorY, +MaxValorY, -ValorAleatorioX, -ValorAleatorioY).
% Genera una posicion aleatoria (X, Y) sin caer dentro de un bloque sólido
% Si existiese un bloque en la posición generada, se llama nuevamente al predicado hasta encontrar una
% posición donde esto no ocurra.
% TODO: Definir el predicado completo.
generarAleatorio(MinValorX, MaxValorX, MinValorY, MaxValorY, 0, 0).

generarAleatorio(MinValorX, MaxValorX, MinValorY, MaxValorY, ValorAleatorioX, ValorAleatorioY):-
    generarAleatorio(MinValorX, MaxValorX, MinValorY, MaxValorY, ValorAleatorioX, ValorAleatorioY).
     
% calcularAccion2(+PacX, +PacY, +FanX, +FanY, -Accion).
% Calcula la acción a realizar por el fantasma de acuerdo a su posición respecto a pacman cuando no se
% encuentran en el mismo eje.
calcularAccion2(PacX, PacY, FanX, FanY, Accion):-
    PacX > FanX,
    PacY < FanY,
    Accion = [arriba, derecha],
    !.
 
calcularAccion2(PacX, PacY, FanX, FanY, Accion):-
    PacX > FanX,
    PacY > FanY,
    Accion = [abajo, derecha],
    !.
 
calcularAccion2(PacX, PacY, FanX, FanY, Accion):-
    PacX < FanX,
    PacY < FanY,
    Accion = [arriba, izquierda],
    !.
 
calcularAccion2(PacX, PacY, FanX, FanY, Accion):-
    PacX < FanX,
    PacY > FanY,
    Accion = [abajo, izquierda],
    !.
 
%%%%%%%%%%%%%%%%%%%%%%%%%%% Comportamiendo Blinky %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

caminoMasCorto([H|Camino], Distancia) :-
    (caminor([H|_], D) -> Distancia < D -> retract(caminor([H|_], _)); true),
    assertz(caminor([H|Camino], Distancia)).

traverse(Desde, Camino, Distancia) :-
    camino(Desde, T, D),
    \+ memberchk(T, Camino),
    caminoMasCorto([T, Desde|Camino], Distancia+D),
    traverse(T, [Desde|Camino], Distancia+D).

traverse(Desde) :-
    retractall(caminor(_, _)),
    traverse(Desde, [], 0).

traverse(_).

% calcAccion(+A, +B, -Accion)
% Devuelve la accion a realizar para pasar de una posicion A a una posicion B. 
calcAcccion([X1, Y1], [X2, Y2], arriba):-X1 = X2, Y1 > Y2, !.
calcAcccion([X1, Y1], [X2, Y2], abajo):-X1 = X2, Y1 < Y2, !.
calcAcccion([X1, Y1], [X2, Y2], izquierda):-Y1 = Y2, X1 > X2, !.
calcAcccion([X1, Y1], [X2, Y2], derecha):-Y1 = Y2, X1 < X2, !.

% caminoAAcciones(+Camino, Acciones)
% Convierte una lista de nodos a una lista de acciones.
caminoAAcciones([_], []):-!.
caminoAAcciones([Pos1,Pos2|T], [Action|Actions]):-
    calcAcccion(Pos1, Pos2, Action),
    caminoAAcciones([Pos2|T], Actions).

% caminoMasCorto(+Desde, +Hacia, -Acciones)
% Encuentra el camino mas corto entre dos puntos y lo devuelve en froma de lista.
caminoMasCorto(Desde, Hacia, Acciones) :-
    traverse(Desde),
    caminor([Hacia|CaminoR], _) ->
        reverse([Hacia|CaminoR], Camino),
        caminoAAcciones(Camino, Acciones).
 
% perseguirBlinky(+PacX, +PacY, +FanX, +FanY, -Accion).
% Indica que acción realizar para Blinky.
% Debe perseguir a pacman cuando se encuentran en el mismo eje X o Y.
perseguirBlinky(PacX, PacY, FanX, FanY, Accion):-
    fantasmaVeAPacman(PacX, PacY, FanX, FanY),
    calcularAccion(PacX, PacY, FanX, FanY, Accion),
    !.
 
% perseguirBlinky(+PacX, +PacY, +FanX, +FanY, -Acciones).
% Indica que acción realizar para Blinky.
% Debe perseguir a pacman desde cualquier punto del mapa.
perseguirBlinky(PacX, PacY, FanX, FanY, Acciones):-
    caminoMasCorto([FanX, FanY], [PacX, PacY], Acciones),
    !.
perseguirBlinky(_, _, _, _, nada).