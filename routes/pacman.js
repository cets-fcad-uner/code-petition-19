var express = require('express');
var router = express.Router();

const swipl = require('swipl');

router.post('/query', function (req, res, next) {
    let call = swipl.call(req.body.query);

    if (!call) {
        res.status(200).send({ success: false, response: 'Algo salió mal' });
    } else {
        if (!call.Actions) {
            res.status(200).send({ success: true, response: call });
        } else {
            res.status(200).send({ success: true, response: call.Actions });
        }
    }
});

module.exports = router;
