import PlayScene from './scenes/playScene.js';
        
let width = 800; //Ancho de la pantalla
let height = 625; //Alto de la pantalla

let config = {
    type: Phaser.CANVAS,
    width: width,
    height: height,
    physics: {
        default: 'arcade', //Utilizando Arcade Physics
        arcade: {
            debug: false,
            gravity: {
                x: 0,
                y: 0
            }            
        }
    },
    scene: [PlayScene]
};

let game = new Phaser.Game(config); //Creo el juego



      