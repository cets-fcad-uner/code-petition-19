/**
 * Define el comportamiento de los Fantasmas del Juego
 */
export default class Ghost {
    /**
     * 
     * @param {Phaser.Scene} scene 
     * @param {Phaser.Geom.Point} position 
     * @param {*} anim 
     */
    constructor(scene, position, anim) {       
        //Creo un sprite con físicas en la posición especificada.
        this.sprite=scene.physics.add.sprite(position.x, position.y, 'ghost')
            .setScale(0.85)
            .setOrigin(0.5);
        
        this.spawnPoint=position; //Establezco la posición en la que aparece el sprite.
        this.anim=anim; //Copio la animación en la propiedad anim.
        this.speed = 100; //Defino la velocidad en 100.
        this.moveTo = new Phaser.Geom.Point(); //Creo un nuevo objeto Point y se lo asigno a moveTo.
        this.safetile = [-1, 19]; //Defino las posiciones inválidas para los fantasmas dentro del juego.
        this.directions = []; //Creo un nuevo array vacío.
        this.opposites = [ null, null, null, null, null, Phaser.DOWN, Phaser.UP, Phaser.RIGHT, Phaser.LEFT ]; //Defino un array con todos los valores opuestos.
        this.turning = Phaser.NONE; //Dirección hacia la que hay que girar.
        this.current = Phaser.NONE; //Dirección actual de fantasma.
        this.turningPoint = new Phaser.Geom.Point(); //Define un objeto punto vacío.
        this.threshold = 5; //Umbral para hacer rotaciones en caso que la posición haya sido pasada
        this.rnd = new Phaser.Math.RandomDataGenerator(); //Creo un nuevo generador de números aleatorios.
        this.sprite.anims.play(anim.Move, true); //Establezco que se reproduzca la animación Move.
        this.turnCount=0; //Pongo a 0 la cantidad de giros del fantasma.
        this.turnAtTime=[4, 8, 16, 32, 64]; //Valores para los cuales se puede doblar aleatoriamente.
        this.turnAt=this.rnd.pick(this.turnAtTime); //Elige uno de los valores del array anterior.
    }

    /**
     * Establece que el fantasma no se debe mover
     */
    freeze() {
        this.moveTo = new Phaser.Geom.Point(); //La dirección hacia la que debe moverse es un nuevo punto
        this.current = Phaser.NONE; //Dirección actual es vacía
    }

    /**
     * Invoca al método move eligiendo aleatoriamente entre las direcciones
     * arriba y abajo.
     */
    move() {
        this.move(this.rnd.pick([Phaser.UP, Phaser.DOWN]));        
    }

    /**
     * De acuerdo a la dirección hacia la que debe moverse el fantasma
     * invoca a los métodos moveLeft(), moveRight(), moveUp() y moveDown()
     * @param {integer} direction 
     */
    move(direction) {
        //Establece la dirección pasada como parámetro como la dirección actual.
        this.current=direction;

        switch(direction) {
            case Phaser.LEFT:
            this.moveLeft();
            break;

            case Phaser.RIGHT:
            this.moveRight();
            break;

            case Phaser.UP:
            this.moveUp();
            break;

            case Phaser.DOWN:
            this.moveDown();
            break;
        }
    }

    /**
     * Ubica el fantasma en la posición donde aparece
     */
    respawn() {       
        //Establece la posición del sprite
        this.sprite.setPosition(this.spawnPoint.x, this.spawnPoint.y);
        //Elijo aleatoriamente entre direcciones arriba y abajo para hacer el movimiento
        this.move(this.rnd.pick([Phaser.UP, Phaser.DOWN]));
        //Reinicio la propiedad flipX
        this.sprite.flipX = false;
    }

    /**
     * Establece para moveTo x = -1 e y = 0. La propiedad flipX se pone a true
     */
    moveLeft() {
        this.moveTo.x=-1;
        this.moveTo.y=0;
        this.sprite.flipX = true;
        this.sprite.angle = 0;
    }

    /**
     * Establece para moveTo x = 1 e y = 0. La propiedad flipX se pone a false
     */
    moveRight() {
        this.moveTo.x=1;
        this.moveTo.y=0;
        this.sprite.flipX = false;
        this.sprite.angle = 0;
    }

    /**
     * Establece para moveTo x = 0 e y = -1
     */
    moveUp() {
        this.moveTo.x=0;
        this.moveTo.y=-1;
        this.sprite.angle = 0;
    }

    /**
     * Establece para moveTo x = 0 e y = 1
     */
    moveDown() {
        this.moveTo.x=0;
        this.moveTo.y=1;
        this.sprite.angle = 0;
    }

    /**
     * Establezco la velocidad del sprite
     */
    update() {
        this.sprite.setVelocity(this.moveTo.x * this.speed,  this.moveTo.y * this.speed);
        this.turn();
        if(this.directions[this.current] && !this.isSafe(this.directions[this.current].index)) {
            this.sprite.anims.play('faceRight', true);  
            this.takeRandomTurn();                  
        }
    }

    /**
     * Copio el array de direcciones pasado por parámetro a directions
     * 
     * @param {Array} directions 
     */
    setDirections(directions) {
        this.directions = directions;
    }

    /**
     * Copio el punto pasado por parámetro a turningPoint
     * 
     * @param {integer} turningPoint 
     */
    setTurningPoint(turningPoint) {
        this.turningPoint=turningPoint;
    }

    /**
     * Establece la dirección hacia la que tiene que rotar el fantasma
     * @param {Phaser.Geom.Point} turnTo 
     */
    setTurn(turnTo) {

        //No continuo si la dirección: 
        // * No está dentro del array directions.
        // * Es igual a la que actualmente está girando.
        // * Es igual a la que se estableció como actual.
        // * No es segura
        if (!this.directions[turnTo] 
            || this.turning === turnTo 
            || this.current === turnTo 
            || !this.isSafe(this.directions[turnTo].index)) {
            return false;
        }

        //Si la dirección hacia la que hay que girar está en el array de opuestos y es opuesta a la actual...
        if(this.opposites[turnTo] && this.opposites[turnTo] === this.current) {
            //Establezco que es la posición hacia la que hay que girar
            this.move(turnTo);
            //Indico que no se encuentra girando actualmente
            this.turning = Phaser.NONE;
            //Vacío turningPoint
            this.turningPoint = new Phaser.Geom.Point();
        } else {
            //Caso contrario establezco que para esa dirección hay que girar.
            this.turning = turnTo;
        }
    }

    /**
     * 
     */
    takeRandomTurn() {

        //Filtro los giros que me llevarían a una posición segura.
        let turns = [];
        for (let i=0; i < this.directions.length; i++) {
            let direction=this.directions[i];
            if(direction) {
                if(this.isSafe(direction.index)) {
                    turns.push(i);                    
                }
            }
        }

        //Me quedo con los giros que no son opuestos al actual
        if(turns.length >= 2) {
            let index=turns.indexOf(this.opposites[this.current]);
            if(index>-1) {
                turns.splice(index, 1);
            }
        }

        //Elijo aleatorioamente entre uno de ellos
        let turn=this.rnd.pick(turns);       
        //Lo marco como la dirección hacia la que hay que girar.
        this.setTurn(turn);
        //Establezco que la cantidad de giros es 0
        this.turnCount=0;
        this.turnAt=this.rnd.pick(this.turnAtTime); //Selecciono la próxima vez que tomaré un giro aleatorio.
    }

    /**
     * Establece una nueva dirección para el fantasma o la marca para realizarla posteriormente.
     */
    turn() {
        //Si la cantidad de giros es iguala turnAt.
        if(this.turnCount===this.turnAt) {
            //Hago un giro aleatorio.
            this.takeRandomTurn();            
        }

        this.turnCount++; //Incremento la cantidad de giros.

        //Si no hay que girar vuelvo corto la ejecución del método.
        if(this.turning === Phaser.NONE) {
            return false;
        }

        // Esto necesita un (threshold) umbral  porque a altas velocidades no se puede girar porque las coordenadas son pasadas por alto
        if (!Phaser.Math.Within(this.sprite.x, this.turningPoint.x, this.threshold) || !Phaser.Math.Within(this.sprite.y, this.turningPoint.y, this.threshold)) {
            return false;
        }        
        
        //Pongo la posición del sprite hacia la dirección de giro.
        this.sprite.setPosition(this.turningPoint.x, this.turningPoint.y);

        //Hago el giro hacia la dirección especificada por turning.
        this.move(this.turning);
        //Pongo turning a None.
        this.turning = Phaser.NONE;
        //Vacío turningPoint.
        this.turningPoint = new Phaser.Geom.Point();

        return true;
    }

    /**
     * Indica si el índice pasado por parámetro corresponde a un tile (mosaico)
     * seguro
     * @param {integer} index 
     */
    isSafe(index) {
        for (let i of this.safetile) {
            if(i===index) return true;
        }

        return false;
    }

    /**
     * Dibuja unos contornos alrededor del Fantasma
     * @param {Phaser.GameObjects.Graphics} graphics 
     */
    drawDebug(graphics)  {        
        let thickness = 4;
        let alpha = 1;
        let color = 0x00ff00;        
        for (var t = 0; t < 9; t++) {
            if (this.directions[t] === null || this.directions[t] === undefined) {
                continue;
            }

            if ( !this.isSafe(this.directions[t].index)) {
                color = 0xff0000;
            } else {
                color = 0x00ff00;
            }

            graphics.lineStyle(thickness, color, alpha);
            graphics.strokeRect(this.directions[t].pixelX, this.directions[t].pixelY, 32, 32);
        }

        color = 0x00ff00;
        graphics.lineStyle(thickness, color, alpha);
        graphics.strokeRect(this.turningPoint.x, this.turningPoint.y, 1, 1);

    }
}