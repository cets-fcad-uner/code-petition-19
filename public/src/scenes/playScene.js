import Player from '../player.js';
import Ghost from "../ghost.js";
import BlockHandler from "../prolog/block-handler.js";
import GhostBehavior from "../prolog/ghost-behavior.js";

export default class PlayScene extends Phaser.Scene {
    constructor () {
        super({ key: 'PlayScene' });

        this.gridSize = 32; //Tamaño de cada celda
        this.offset = parseInt(this.gridSize / 2); //centro de cada celda
        this.cursors;
        this.player;
        this.ghosts = [];
        this.pills;
        this.pillsCount = 0;
        this.pillsAte = 0;
        this.map;
        this.layer1;
        this.layer2;
        this.graphics;
        this.scoreText;
        this.livesImage = [];
        this.tiles = "pacman-tiles";
        this.spritesheet = 'pacman-spritesheet'; //Nombre del SpriteSheet
        this.spritesheetPath = 'assets/images/pacmansprites.png'; //Sprites de Pac-Man y los fantasmas.
        this.tilesPath = 'assets/images/background.png'; //Sprites del mapa.
        this.mapPath = 'assets/levels/codepen-level.json'; //Cargo el JSON del mapa.
        this.Animation = { //Defino las animaciones de Pac-Man.
            Player: {
                Eat: 'player-eat',
                Stay: 'player-stay',
                Die: 'player-die'
            },
            Ghost: { //Defino las animaciones. 1 para cada fantasma.
                Pink: { Move: 'ghost-pink-move', },
                Red: { Move: 'ghost-red-move', },
                Blue: { Move: 'ghost-blue-move', },
                Orange: { Move: 'ghost-orange-move', }
            }
        };


        /*
            COMENTAR LAS SIGUIENTES LÍNEAS SI NO SE USA PROLOG PARA EL MOV. DE LOS FANTASMAS
            */
        this.blockHandler;
        this.ghostBehavior = new GhostBehavior(this.Animation);
        this.activePlayer = false;
        window.onkeydown = () => {
            this.activePlayer = true;
        };
    }


    /**
     * Función preload invocada por Phaser
     */
    preload() {
        //Cargo los sprites desde la dirección especificada por spritesheetPath (el ancho y alto de cada celda está determinado por gridSize)
        this.load.spritesheet(this.spritesheet, this.spritesheetPath, { frameWidth: this.gridSize, frameHeight: this.gridSize });
        //Cargo el path al mapa
        this.load.tilemapTiledJSON("map", this.mapPath);
        this.load.image(this.tiles, this.tilesPath);
        //Cargo la imagen de las píldoras pequeñas
        this.load.image("pill", "assets/images/pacman-pill/spr_pill_0.png");
        //Cargo la imagen con la que hago referencia a la cantidad de vidas restantes de Pac-Man
        this.load.image("lifecounter", "assets/images/pacman-life-counter/spr_lifecounter_0.png");
    }

    /**
     * Función create invocada por Phaser 
     */
    create() {
        //Definición de animación de Pac-Man cuando come (comprenden los frames 9 - 13)
        this.anims.create({
            key: this.Animation.Player.Eat,
            frames: this.anims.generateFrameNumbers(this.spritesheet, { start: 9, end: 13 }),
            frameRate: 10,
            repeat: -1
        });

        //Definición de animación de Pac-Man detenido (solo frame 9)
        this.anims.create({
            key: this.Animation.Player.Stay,
            frames: [{ key: this.spritesheet, frame: 9 }],
            frameRate: 20
        });

        //Definición de animación de Pac-Man cuando muere (comprenden los frames 6 - 8)
        this.anims.create({
            key: this.Animation.Player.Die,
            frames: this.anims.generateFrameNumbers(this.spritesheet, { start: 6, end: 8 }),
            frameRate: 1
        });

        //Definición de animación del fantasma Celeste (Inky) cuando se mueve (frames 0 - 1)
        this.anims.create({
            key: this.Animation.Ghost.Blue.Move,
            frames: this.anims.generateFrameNumbers(this.spritesheet, { start: 0, end: 1 }),
            frameRate: 10,
            repeat: -1
        });

        //Definición de animación del fantasma Naranja (Clyde) cuando se mueve (frames 4 - 5)
        this.anims.create({
            key: this.Animation.Ghost.Orange.Move,
            frames: this.anims.generateFrameNumbers(this.spritesheet, { start: 4, end: 5 }),
            frameRate: 10,
            repeat: -1
        });

        //Definición de animación de fantasma Rosa (Pinky). Frames (14 - 15)
        this.anims.create({
            key: this.Animation.Ghost.Pink.Move,
            frames: this.anims.generateFrameNumbers(this.spritesheet, { start: 14, end: 15 }),
            frameRate: 10,
            repeat: -1
        });

        //Definición de animación de fantasma Rojo (Blinky). Frames (16 - 17)
        this.anims.create({
            key: this.Animation.Ghost.Red.Move,
            frames: this.anims.generateFrameNumbers(this.spritesheet, { start: 16, end: 17 }),
            frameRate: 10,
            repeat: -1
        });

        //Construyo un mapa de mosaicos y se lo asigno a map
        this.map = this.make.tilemap({ key: "map", tileWidth: this.gridSize, tileHeight: this.gridSize });

        // COMENTAR LAS SIGUIENTES LÍNEAS SI NO SE USA PROLOG PARA EL MOV. DE LOS FANTASMAS
        this.blockHandler = new BlockHandler(this.map);
        
        //Establezco las imagenes del mapa
        const tileset = this.map.addTilesetImage(this.tiles);

        //Creo la capa de mapa estático correspondiente al laberinto 
        this.layer1 = this.map.createStaticLayer("Layer 1", tileset, 0, 0);
        //Establezco que con las paredes del mapa se produce colisión
        this.layer1.setCollisionByProperty({ collides: true });

        //Creo la capa de mapa estático correspondiente a la barrera roja de la jaula de los fantasmas.
        this.layer2 = this.map.createStaticLayer("Layer 2", tileset, 0, 0);
        //Establezco que con las paredes del mapa se produce colisión
        this.layer2.setCollisionByProperty({ collides: true });

        //Obtengo el punto donde aparece el Pac-Man en el mapa
        let spawnPoint = this.map.findObject("Objects", obj => obj.name === "Player");
        //Calculo la ubicación central dentro del punto de aparición del Pac-Man en el mapa.
        let position = new Phaser.Geom.Point(spawnPoint.x + this.offset, spawnPoint.y - this.offset);

        //Creo un objeto Player en la posición calculada anteriormente, 
        //las animaciones definidas y una función de callback para que 
        //cuando muera Pac-Man si no le quedan más vidas comience un nuevo juego
        //caso contrario va a ser reubicado en la posición donde debe aparecer.
        this.player = new Player(this, position, this.Animation.Player, () => {
            //Si la cantidad de vidas es menor o igual a 0
            if (this.player.life <= 0) {
                this.newGame(); //Comienza un nuevo juego
            } else {
                this.respawn(); //Se reubican los fantasmas y Pac-Man
            }

        });

        //Guardo en scene la referencia a esta escena
        let scene = this;

        // COMENTAR LAS SIGUIENTES LÍNEAS SI NO SE USA PROLOG PARA EL MOV. DE LOS FANTASMAS
        this.blockHandler.setBlocks();

        //Defino el grupo pills (píldoras)
        this.pills = this.physics.add.group();

        //Para cada uno de los objetos del mapa...
        this.map.filterObjects("Objects", (value) => {
            //Si son píldoras
            if (value.name == "Pill") {
                //Agrego el sprite correspondiente a la píldora en el centro de cada celda
                let pill = scene.physics.add.sprite(value.x + this.offset, value.y - this.offset, "pill");
                //Agrego la píldora al grupo pills.
                this.pills.add(pill);
                //Acumulo en pillsCount la cantidad de píldoras
                this.pillsCount++;
            }
        });

        //Creo el grupo de fantasmas ghostGroup
        let ghostsGroup = this.physics.add.group();
        let i = 0;
        //Defino un array con cada una de las animaciones de los fantasmas
        let skins = [this.Animation.Ghost.Blue, this.Animation.Ghost.Red, this.Animation.Ghost.Orange, this.Animation.Ghost.Pink];

        //Para cada objeto del mapa
        this.map.filterObjects("Objects", (value) => {
            //Si se trata de un fantasma
            if (value.name == "Ghost") {
                //Calculo la posición en la que debería aparecer en el mapa
                let position = new Phaser.Geom.Point(value.x + this.offset, value.y - this.offset);
                //Creo un nuevo fantasma y lo referencio con ghost
                let ghost = new Ghost(scene, position, skins[i]);
                //Agrego el fantasma al array de fantasmas ghosts
                this.ghosts.push(ghost);
                //Agrego el fantasma al grupo 
                ghostsGroup.add(ghost.sprite);
                i++;
            }
        });

        //Establezco que el Pac-Man colisiona con las paredes del laberinto
        this.physics.add.collider(this.player.sprite, this.layer1);
        //Establezco que el Pac-Man colisiona con la barrera roja de la jaula de los fantasmas
        this.physics.add.collider(this.player.sprite, this.layer2);
        //Establezco que todos los fantasmas colisionan con las paredes del laberinto
        this.physics.add.collider(ghostsGroup, this.layer1);

        //Defino qué sucede cuando el Pac-Man se superpone con una píldora
        this.physics.add.overlap(this.player.sprite, this.pills, function (sprite, pill) {
            //Desactivo la píldora en el juego y también la oculto
            pill.disableBody(true, true);
            //Incremento el contador del píldoras comidas
            this.pillsAte++;
            //Incremento el puntaje del jugador en 10
            this.player.score += 10;
            //Si la cantidad de píldoras comidas es igual al total de píldoras
            if (this.pillsCount == this.pillsAte) {
                //Reinicio el juego
                this.reset();
            }
        }, null, this);

        //Si colisiona Pac-Man y algún fantasma
        this.physics.add.overlap(this.player.sprite, ghostsGroup, (sprite, ghostSprite) => {
            //Si el jugador está activo...
            if (this.player.active) {
                //Pac-Man muere
                this.player.die();
                //Todos los fantasmas se detienen.
                for (let ghost of this.ghosts) {
                    ghost.freeze();
                }
            }
        }, null, this);

        //Gestiono los eventos del teclado con las flechas del teclado
        this.cursors = this.input.keyboard.createCursorKeys();

        this.graphics = this.add.graphics();

        //Escribo los textos Puntos y vidas
        this.scoreText = this.add.text(25, 595, 'Puntos: ' + this.player.score).setFontFamily('Arial').setFontSize(18).setColor('#ffffff');
        this.add.text(630, 595, 'Vidas:').setFontFamily('Arial').setFontSize(18).setColor('#ffffff');
        //Dibujo el contador de vidas por pantalla
        for (let i = 0; i < this.player.life; i++) {
            this.livesImage.push(this.add.image(700 + (i * 25), 605, 'lifecounter'));
        }
    }

    /**
     * Función que reubica a Pac-Man y los fantasmas nuevamente en el mapa
     */
    respawn() {
        //Reubica a Pac-Man
        this.player.respawn();

        //Reubica a los Fantasmas
        for (let ghost of this.ghosts) {
            ghost.respawn();
        }
    }

    /**
     * Reinicia el juego. 
     * Se invoca cuando el Jugador gana el juego o cuando pierde todas las vidas.
     */
    reset() {
        //Reubica al Pac-Man y a los fantasmas en el mapa
        this.respawn();
        //Activa cada una de las píldoras y las vuelve a mostrar
        for (let child of this.pills.getChildren()) {
            child.enableBody(false, child.x, child.y, true, true);
        }
        //Establezco que la cantidad de píldoras comidas es 0
        this.pillsAte = 0;
    }

    /**
     * Inicia un nueva nueva partida. Se invoca cuando el jugador pierde todas
     * las vidas 
     */
    newGame() {
        //Vuelve a ubicar a Pac-Man y a los fantasmas en el mapa.
        //Hace aparecer nuevamente en el mapa las píldoras.
        this.reset();
        //Define que el jugador tiene 3 vidas.
        this.player.life = 3;
        //Reinicia el puntaje a 0.
        this.player.score = 0;
        //Redibuja las vidas del jugador completamente opacas.
        for (let i = 0; i < this.player.life; i++) {
            let image = this.livesImage[i];
            if (image) {
                image.alpha = 1;
            }
        }
    }

    /**
     * Función update invocada por Phaser
     */
    update() {
        //Estabezco cuáles son las direcciones hacia las que podría moverse Pac-Man en el laberinto
        this.player.setDirections(this.getDirection(this.map, this.layer1, this.player.sprite));

        //Si el juego fue interrumpido -> los fantasmas se detienen
        if (!this.player.playing) {
            for (let ghost of this.ghosts) {
                ghost.freeze();
            }
        }

        //Determino las direcciones hacia las que podrían moverse los fantasmas en el laberinto
        for (let ghost of this.ghosts) {
            ghost.setDirections(this.getDirection(this.map, this.layer1, ghost.sprite));
        }

        //Establezco cuál es el punto de giro de Pac-Man
        this.player.setTurningPoint(this.getTurningPoint(this.map, this.player.sprite));

        
        /*
            DESCOMENTAR LAS SIGUIENTES LÍNEAS EN CASO DE NO UTILIZAR PROLOG PARA CONTROLAR EL MOV. DE LOS FANTASMAS
            //Establezco cuál es el punto de giro de los fantasmas
            for(let ghost of this.ghosts){
                ghost.setTurningPoint(this.getTurningPoint(this.map, ghost.sprite));
            }
        */

        /*
            COMENTAR LAS SIGUIENTES 2 LÍNEAS EN CASO DE NO UTILIZAR PROLOG PARA CONTROLAR EL MOV. DE LOS FANTASMAS
        */
        this.ghostBehavior.updateGhosts(this);
        this.ghostBehavior.incrementIfps(this.activePlayer);

        //De acuerdo a las teclas presionadas por el jugador giro el Pac-Man
        if (this.cursors.left.isDown) {
            this.player.setTurn(Phaser.LEFT);
        } else if (this.cursors.right.isDown) {
            this.player.setTurn(Phaser.RIGHT);
        } else if (this.cursors.up.isDown) {
            this.player.setTurn(Phaser.UP);
        } else if (this.cursors.down.isDown) {
            this.player.setTurn(Phaser.DOWN);
        } else {
            this.player.setTurn(Phaser.NONE);
        }

        //Invoco a la función update() de Pac-Man
        this.player.update();

        //Invoco a la función update() de cada uno de los fantasmas
        for (let ghost of this.ghosts) {
            ghost.update();
        }

        //Actualizo el puntaje
        this.scoreText.setText('Score: ' + this.player.score);

        //Dibujo las imágenes correspondientes a la cantidad de vidas restantes.
        for (let i = this.player.life; i < 3; i++) {
            let image = this.livesImage[i];
            if (image) {
                image.alpha = 0;
            }
        }

        //Si el jugador está activo y pasa por los pasillos laterales (sin cierre) del laberinto
        //Entonces se redibuja en el otro extremo.
        if (this.player.active) {
            if (this.player.sprite.x < 0 - this.offset) {
                this.player.sprite.setPosition(this.sys.game.config.width + this.offset, this.player.sprite.y);
            } else if (this.player.sprite.x > this.sys.game.config.width + this.offset) {
                this.player.sprite.setPosition(0 - this.offset, this.player.sprite.y);
            }
        }


        //drawDebug();
    }

    /**
     * Función de depuración que permite dibujar las celdas hacia 
     * las que Pac-Man y los fantasmas se pueden mover y las que no.
     */
    drawDebug() {
        //limpia lo dibujado en la iteración anterior
        this.graphics.clear();

        //Dibuja los límites del Pac-Man
        this.player.drawDebug(this.graphics);

        //Dibuja los límites de los fantasmas
        for (let ghost of this.ghosts) {
            ghost.drawDebug(this.graphics);
        }
    }

    /**
     * Para un mapa, capa y sprite obtiene un array con las posiciones del mapa
     * hacia las que podría moverse en todas las direcciones.
     * 
     * @param {Phaser.Tilemaps.Tilemap} map 
     * @param {Phaser.Tilemaps.StaticTilemapLayer} layer 
     * @param {Phaser.GameObjects. Sprite} sprite 
     */
    getDirection(map, layer, sprite) {
        let directions = [];
        let sx = Phaser.Math.FloorTo(sprite.x); //Determino la celda en X
        let sy = Phaser.Math.FloorTo(sprite.y); //Determino la celda en Y
        let currentTile = map.getTileAtWorldXY(sx, sy, true); //Obtengo el mosaico actual

        if (currentTile) {
            var x = currentTile.x;
            var y = currentTile.y;
            //Obtengo el mosacio en las direcciones y las guardo en cada una de las posiciones del array.
            directions[Phaser.LEFT] = map.getTileAt(x - 1, y, true, layer);
            directions[Phaser.RIGHT] = map.getTileAt(x + 1, y, true, layer);
            directions[Phaser.UP] = map.getTileAt(x, y - 1, true, layer);
            directions[Phaser.DOWN] = map.getTileAt(x, y + 1, true, layer);
        }

        return directions;
    }

    /**
     * Determina el punto central del sprite en el mapa.
     * @param {Phaser.Tilemaps.Tilemap} map 
     * @param {Phaser.GameObjects.Sprite} sprite 
     */
    getTurningPoint(map, sprite) {
        let turningPoint = new Phaser.Geom.Point();
        let sx = Phaser.Math.FloorTo(sprite.x);
        let sy = Phaser.Math.FloorTo(sprite.y);
        let currentTile = map.getTileAtWorldXY(sx, sy, true);
        if (currentTile) {
            turningPoint.x = currentTile.pixelX + this.offset;
            turningPoint.y = currentTile.pixelY + this.offset;
        }

        return turningPoint;
    }
}