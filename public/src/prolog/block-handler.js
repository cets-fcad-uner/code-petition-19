import PrologService from '../services/prolog.js';

export default class BlockHandler {

    constructor (map) {
        this.map = map;
        this.prologService = new PrologService();
    }

    async setBlocks() {
        let bloques = [];

        let query = "";

        // agregar bloques a la db
        this.map.filterTiles((tile) => {
            // console.log(":- agregarBloque(" + (tile.x) + ", " + (tile.y) + ").");
            bloques.push({ x: tile.x, y: tile.y });
            query += "agregarBloque(" + (tile.x) + ", " + (tile.y) + "), ";
        }, null, 0, 0, 25, 18, { isNotEmpty: true }, "Layer 1");

        // agregar bloques libres a la bd
        this.map.filterTiles((tile) => {
            if (bloques.filter(b => b.x === tile.x && b.y === tile.y).length == 0) {
                // console.log(":- agregarBloqueLibre(" + (tile.x) + ", " + (tile.y) + ").");
                query += "agregarBloqueLibre(" + (tile.x) + ", " + (tile.y) + "), ";
            }
        }, null, 0, 0, 25, 18, {}, "Layer 1");

        query += "generarAdyacentes.";

        // agregar adyacentes a la bd
        // console.log(":- generarAdyacentes.")
        await this.prologService.plQuery(query);
    }

}