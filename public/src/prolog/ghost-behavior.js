import PrologService from '../services/prolog.js';

export default class GhostBehavior {
    constructor (Animation) {
        this.prologService = new PrologService();
        this.Animation = Animation;
    
        this.intelligentGhosts = [
            this.Animation.Ghost.Orange,
            this.Animation.Ghost.Red,
            this.Animation.Ghost.Blue,
            this.Animation.Ghost.Pink
        ];
    }

    plFps = 60;
    plFpsLento = 60 * 10;
    plActive = true;
    plClydePersiguiendo = false;
    plClydeAccion = '';
    plBlinkyPersiguiendo = false;
    plBlinkyAccion = '';
    plInkyPersiguiendo = false;
    plInkyAccion = '';
    plPinkyPersiguiendo = false;
    plPinkyAccion = '';
    plBlinkyAcciones = [];
    ifps = 0;

    getTurn(accion) {
        let turn = 0;

        switch (accion) {
            case 'arriba':
                turn = Phaser.UP;
                break;
            case 'abajo':
                    turn = Phaser.DOWN;
                break;
            case 'izquierda':
                    turn = Phaser.LEFT;
                break;
            case 'derecha':
                    turn = Phaser.RIGHT;
                break;
        }

        return turn;
    }

    plActionsToArray(actions, arr) {
        arr.push(actions.head);

        if (actions.tail != '[]') {
            this.plActionsToArray(actions.tail, arr);
        }

        return arr;
    }

    incrementIfps(playerActive) {
        if (playerActive) {
            this.ifps++;
        }
    }

    updateGhosts(playScene) {

        // bucle rápido
        if (this.plActive && this.ifps > 300 && this.ifps % this.plFps == 0) {
            this.plClydePersiguiendo = false;
            this.plInkyPersiguiendo = false;
            this.plPinkyPersiguiendo = false;

            let activarBlinky = this.ifps % this.plFpsLento == 0;
            if (activarBlinky) {
                this.plBlinkyPersiguiendo = false;
                this.plBlinkyAcciones = [];
            }

            let predicados = [];
            for (let ghost of playScene.ghosts) {
                if (this.intelligentGhosts.filter((g) => { return g == ghost.anim; }).length > 0) {
                    let playerTile = playScene.map.getTileAtWorldXY(playScene.player.sprite.x, playScene.player.sprite.y, true);
                    let ghostTile = playScene.map.getTileAtWorldXY(ghost.sprite.x, ghost.sprite.y, true);

                    if (playerTile != null && ghostTile != null) {
                        switch(ghost.anim) {
                            case this.Animation.Ghost.Pink:
                                predicados.push("perseguirPinky(" + (playerTile.x) + ", " + (playerTile.y) + ", " + (ghostTile.x) + ", " + (ghostTile.y) + ", AccionPinky)");
                                break;
                            case this.Animation.Ghost.Red:
                                if (activarBlinky) {
                                    predicados.push("perseguirBlinky(" + (playerTile.x) + ", " + (playerTile.y) + ", " + (ghostTile.x) + ", " + (ghostTile.y) + ", AccionBlinky)");
                                }
                                break;
                            case this.Animation.Ghost.Blue:
                                predicados.push("perseguirInky(" + (playerTile.x) + ", " + (playerTile.y) + ", " + (ghostTile.x) + ", " + (ghostTile.y) + ", AccionInky)");
                                break;
                            case this.Animation.Ghost.Orange:
                                predicados.push("perseguirClyde(" + (ghostTile.x) + ", " + (ghostTile.y) + ", AccionClyde)");
                                break;
                        }
                    }
                }
            }

            if (predicados.length) {
                const predicadosStr = predicados.toString() + "." + "\n";
                // console.log(':- ' + predicadosStr);
                return this.prologService.plQuery(predicadosStr).then((res) => {
                    
                    if (res && res.response && activarBlinky && res.response.AccionBlinky != 'nada') {
                        if (typeof res.response.AccionBlinky != 'string') {
                            this.plBlinkyPersiguiendo = true;
                            // console.log('%c ' + this.plActionsToArray(res.response.AccionBlinky, []),
                            //     'background-color: black; color: red; font-weight: bold;');
                            this.plBlinkyAcciones = this.plActionsToArray(res.response.AccionBlinky, []);
                        } else if (res.response.AccionBlinky != 'nada') {
                            this.plBlinkyPersiguiendo = true;
                            // console.log('%c Blinky persiguiendo! -> %c' + res.response.AccionBlinky + ' ',
                            //     'background-color: black; color: red', 'background-color: black; color: red; font-weight: bold;');
                            for (let ghost of playScene.ghosts) {
                                if (ghost.anim == this.Animation.Ghost.Red) {
                                    ghost.setTurningPoint(playScene.getTurningPoint(playScene.map, ghost.sprite));
                                    ghost.setTurn(this.getTurn(res.response.AccionBlinky));
                                }
                            }
                        }
                    }

                    if (res && res.response && res.response.AccionClyde != 'nada') {
                        // console.log('%c Clyde persiguiendo! -> %c' + res.response.AccionClyde + ' ',
                        //     'background-color: black; color: orange', 'background-color: black; color: orange; font-weight: bold;');
                        this.plClydePersiguiendo = true;
                        for (let ghost of playScene.ghosts) {
                            if (ghost.anim == this.Animation.Ghost.Orange) {
                                ghost.setTurningPoint(playScene.getTurningPoint(playScene.map, ghost.sprite));
                                ghost.setTurn(this.getTurn(res.response.AccionClyde));
                            }
                        }
                    }

                    if (res && res.response && res.response.AccionInky != 'nada') {
                        // console.log('%c Inky persiguiendo! -> %c' + res.response.AccionInky + ' ',
                        //     'background-color: black; color: cyan', 'background-color: black; color: cyan; font-weight: bold;');
                        this.plInkyPersiguiendo = true;
                        for (let ghost of playScene.ghosts) {
                            if (ghost.anim == this.Animation.Ghost.Blue) {
                                ghost.setTurningPoint(playScene.getTurningPoint(playScene.map, ghost.sprite));
                                ghost.setTurn(this.getTurn(res.response.AccionInky));
                            }
                        }
                    }

                    if (res && res.response && res.response.AccionPinky != 'nada') {
                        // console.log('%c Pinky persiguiendo! -> %c' + res.response.AccionPinky + ' ',
                        //     'background-color: black; color: pink', 'background-color: black; color: pink; font-weight: bold;');
                        this.plPinkyPersiguiendo = true;
                        for (let ghost of playScene.ghosts) {
                            if (ghost.anim == this.Animation.Ghost.Pink) {
                                ghost.setTurningPoint(playScene.getTurningPoint(playScene.map, ghost.sprite));
                                ghost.setTurn(this.getTurn(res.response.AccionPinky));
                            }
                        }
                    }
                });
            }
        }
    
        // bucle blinky
        if (this.plActive && this.ifps > 300 && this.ifps % 18 == 0 && this.plBlinkyPersiguiendo && this.plBlinkyAcciones.length > 0) {
            // console.log(this.plBlinkyAcciones);
            let accionBlinky = this.plBlinkyAcciones.shift();
            // console.log('%c Blinky persiguiendo! -> %c' + accionBlinky + ' ',
            //     'background-color: black; color: red', 'background-color: black; color: red; font-weight: bold;');
            for (let ghost of playScene.ghosts) {
                if (ghost.anim == this.Animation.Ghost.Red) {
                    ghost.setTurningPoint(playScene.getTurningPoint(playScene.map, ghost.sprite));
                    ghost.setTurn(this.getTurn(accionBlinky));
                }
            }
            this.plBlinkyAccion = accionBlinky;
        }

        for (let ghost of playScene.ghosts) {
            if (this.intelligentGhosts.filter((g) => { return g == ghost.anim; }).length == 0 ||
                (ghost.anim == this.Animation.Ghost.Orange && !this.plClydePersiguiendo) ||
                (ghost.anim == this.Animation.Ghost.Red && !this.plBlinkyPersiguiendo) ||
                (ghost.anim == this.Animation.Ghost.Blue && !this.plInkyPersiguiendo) ||
                (ghost.anim == this.Animation.Ghost.Pink && !this.plPinkyPersiguiendo)) {
                ghost.setTurningPoint(playScene.getTurningPoint(playScene.map, ghost.sprite));
            } else if (ghost.anim == this.Animation.Ghost.Orange && this.plClydePersiguiendo) {
                ghost.setTurningPoint(playScene.getTurningPoint(playScene.map, ghost.sprite));
                ghost.setTurn(this.getTurn(this.plClydeAccion));
            } else if (ghost.anim == this.Animation.Ghost.Red && this.plBlinkyPersiguiendo) {
                ghost.setTurningPoint(playScene.getTurningPoint(playScene.map, ghost.sprite));
                ghost.setTurn(this.getTurn(this.plBlinkyAccion));
            } else if (ghost.anim == this.Animation.Ghost.Blue && this.plInkyPersiguiendo) {
                ghost.setTurningPoint(playScene.getTurningPoint(playScene.map, ghost.sprite));
                ghost.setTurn(this.getTurn(this.plInkyAccion));
            } else if (ghost.anim == this.Animation.Ghost.Pink && this.plPinkyPersiguiendo) {
                ghost.setTurningPoint(playScene.getTurningPoint(playScene.map, ghost.sprite));
                ghost.setTurn(this.getTurn(this.plPinkyAccion));
            }
        }
    }

}
