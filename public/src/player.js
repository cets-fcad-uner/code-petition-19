/**
 * Define animaciones y comportamiento del Pac-Man 
 */
export default class Player {
    /**
     * Recibe por parámetro la escena actual, la posición de inicio, las animaciones y 
     * la función callback a ejecutar cuando muere.
     * 
     * @param {Phaser.Scene} scene 
     * @param {Phaser.Geom.Point} position 
     * @param {Phaser.GameObjects.Components.Animation} anim 
     * @param {function} dieCallback 
     */
    constructor(scene, position, anim, dieCallback) {       
        //Crea el sprite
        this.sprite = scene.physics.add.sprite(position.x, position.y, 'pacman')
            .setScale(0.9)
            .setOrigin(0.5);

        
        this.spawnPoint = position; //Guardo la posición de inicio.
        this.anim = anim; //Guardo las animaciones.
        this.dieCallback = dieCallback; //Guardo la función de callback.
        this.sprite.angle = 180; //Configuro el ángulo inicial de Pac-Man.
        this.speed = 95; //Establezco la velocidad de movimiento de Pac-Man.
        this.moveTo = new Phaser.Geom.Point(); //Indica el punto hacia el que moverse.
        this.safetile = [-1, 18]; // Indica en X y en Y cuales son los mosaicos no seguros.
        this.directions = []; //Construyo el array de direcciones.
        this.opposites = [ null, null, null, null, null, Phaser.DOWN, Phaser.UP, Phaser.RIGHT, Phaser.LEFT ]; //Defino un array con todos los valores opuestos
        this.turning = Phaser.NONE; //Dirección hacia la que hay que girar
        this.current = Phaser.NONE; //Dirección actual del Pac-Man
        this.turningPoint = new Phaser.Geom.Point(); ///**** Ver ****
        this.threshold = 5; //Umbral para hacer rotaciones en caso que la posición haya sido pasada
        this.life = 3; //Establezco la cantidad inicial de vidas.
        this.score=0; //Establezco el puntaje inicial.
        this.active=true; //Establezco el estado activo.
        this.sprite.anims.play(this.anim.Stay, true); //Establezco como animación actual la de Pac-Man detenido (stay).
        let ref=this; //Guardo la referencia al objeto actual.
        this.sprite.on('animationcomplete', function(animation, frame) {
            ref.animComplete(animation, frame);
        }, scene); //Establezco que cuando finalice una animación se ejecute una llamada a animComplete().
        this.playing = false; //Establezco que actualmente no ha comenzado a jugar.
    }

    /**
     * Establece qué sucede cuando Pac-Man muere
     */
    die() {
        this.active = false; //Pac-Man deja de estar activo.
        this.playing = false; //Se detiene el juego
        this.life--; //Se decrementa la cantidad de vidas
        this.moveTo = new Phaser.Geom.Point(); //Punto al que moverse en la próxima iteración.
        this.sprite.anims.play(this.anim.Die, true); //Animación a ejecutar.
    }

    /**
     * Cuando finaliza la animación de la muerte de Pac-Man ejecuta la función de callback
     * @param {Phaser.GameObjects.Components. Animation} animation 
     * @param {integer} frame 
     */
    animComplete (animation, frame) {        
        if(animation.key==this.anim.Die) {
            this.dieCallback();
        }
    }

    /**
     * Acciones para reiniciar el Pac-Man cuando muere
     */
    respawn() {
        this.active = true; //Estado activo
        this.playing = false; //Actualmente no jugando
        this.sprite.setPosition(this.spawnPoint.x, this.spawnPoint.y); //Posición de inicio
        this.moveTo = new Phaser.Geom.Point(); //Punto para moverse a continuación no establecido
        this.sprite.anims.play(this.anim.Stay, true); //Animaciones => Pac-Man quieto
        this.sprite.angle = 180; //Ángulo de rotación inicial
        this.turning = Phaser.NONE; //Dirección hacia la que hay que girar
        this.current = Phaser.NONE;
    }

    /**
     * Establece cómo Pac-Man se mueve la izquierda
     */
    moveLeft() {
        this.moveTo.x=-1;
        this.moveTo.y=0;
        this.sprite.anims.play(this.anim.Eat, true);
        this.sprite.angle = 180;
    }

    /**
     * Establece cómo Pac-Man se mueve a la derecha
     */
    moveRight() {
        this.moveTo.x=1;
        this.moveTo.y=0;
        this.sprite.anims.play(this.anim.Eat, true);
        this.sprite.angle = 0;
    }

    /**
     * Establece cómo Pac-Man se mueve hacia arriba
     */
    moveUp() {
        this.moveTo.x=0;
        this.moveTo.y=-1;
        this.sprite.anims.play(this.anim.Eat, true);
        this.sprite.angle = 270;
    }

    /**
     * Establece cómo Pac-Man se mueve hacia abajo
     */
    moveDown() {
        this.moveTo.x=0;
        this.moveTo.y=1;
        this.sprite.anims.play(this.anim.Eat, true);
        this.sprite.angle = 90;
    }

    /**
     * Actualiza la posición y el movimiento de giro de Pac-Man
     */
    update() {
        //Establece la velocidad de Pac-Man
        this.sprite.setVelocity(this.moveTo.x * this.speed,  this.moveTo.y * this.speed);

        //Ejecuta si corresponde el giro del sprite
        this.turn();

        //Si moverse a la siguiente posición implica caer dentro de una pared se detiene la animación.
        if(this.directions[this.current] && !this.isSafe(this.directions[this.current].index)) {
            this.sprite.anims.play('faceRight', true);
        }
    }

    /**
     * Establece la propiedad directions
     * @param {integer} directions 
     */
    setDirections(directions) {
        this.directions = directions;
    }

    /**
     * Establece la propiedad turningPoint
     * @param {Phaser.Geom.Point} turningPoint 
     */
    setTurningPoint(turningPoint) {
        this.turningPoint=turningPoint;
    }


    /**
     * Establece la dirección hacia la que el jugador decidió girar.
     * @param {integer} turnTo 
     */
    setTurn(turnTo) {
        //No se ejecuta cuando:
        // * El jugador no está activo.
        // * La dirección no existe.
        // * Ya está girando en esa dirección
        // * Ya giró en esa dirección
        // * El mosaico destino no es seguro
        if (!this.active || !this.directions[turnTo] || this.turning === turnTo || this.current === turnTo || !this.isSafe(this.directions[turnTo].index)) {
            return false;
        }

        //Si la dirección a moverse es opuesta a la actual -> hace el giro
        if(this.opposites[turnTo] && this.opposites[turnTo] === this.current) {
            this.move(turnTo);
            this.turning = Phaser.NONE;
            this.turningPoint = new Phaser.Geom.Point();
        } else {
            //Caso contrario lo deja agendado para hacerlo en la siguiente llamada a update()
            this.turning = turnTo;
         }
    }


    /**
     * Acciones a ejecutar para girar Pac-Man
     */
    turn() {

        //Si no está girando no ejecuto lo que continúa
        if(this.turning === Phaser.NONE) {
            return false;
        }

        // Esto necesita un umbral porque a altas velocidades no se ejecuta el giro porque las coordenadas son pasadas por alto
        if (!Phaser.Math.Within(this.sprite.x, this.turningPoint.x, this.threshold) || !Phaser.Math.Within(this.sprite.y, this.turningPoint.y, this.threshold)) {
            return false;
        }        

        //Establece la posición
        this.sprite.setPosition(this.turningPoint.x, this.turningPoint.y);
        
        this.move(this.turning);

        //Indica que ya no está girando
        this.turning = Phaser.NONE;
        //Reinicia turning point
        this.turningPoint = new Phaser.Geom.Point();

        return true;
    }

    /**
     *  De acuerdo al parámetro direction establece cuál es sentido del giro que tiene que ejecutar
     * @param {integer} direction 
     */
    move(direction) {
        this.playing = true; //Indica que está jugando
        this.current = direction; //Establece la dirección actual

        switch(direction) {
            case Phaser.LEFT:
            this.moveLeft();
            break;

            case Phaser.RIGHT:
            this.moveRight();
            break;

            case Phaser.UP:
            this.moveUp();
            break;

            case Phaser.DOWN:
            this.moveDown();
            break;
        }
    }

    /**
     * Indica si se puede mover al mosaico que sigue
     * @param {integer} index 
     */
    isSafe(index) {
        for (let i of this.safetile) {
            if(i===index) return true;
        }

        return false;
    }

    /**
     * Dibuja unos contornos alrededor de Pac-Man
     * @param {Phaser.GameObjects.Graphics} graphics 
     */
    drawDebug(graphics) {
        let thickness = 4;
        let alpha = 1;
        let color = 0x00ff00;

        //Para las 9 direcciones 
        for (var t = 0; t < 9; t++) {
            //Si la dirección existe
            if (this.directions[t] === null || this.directions[t] === undefined) {
                continue;
            }

            //Si no se puede mover a la dirección especificada -> color rojo
            //Caso contrario -> color verde
            if (this.directions[t].index !== -1) {
                color = 0xff0000;
            } else {
                color = 0x00ff00;
            }

            graphics.lineStyle(thickness, color, alpha); //establezco las características de los bordes.
            graphics.strokeRect(this.directions[t].pixelX, this.directions[t].pixelY, 32, 32); //Dibujo un rectángulo
        }

        color = 0x00ff00; //Color verde
        graphics.lineStyle(thickness, color, alpha); //establezco las características de los bordes.
        graphics.strokeRect(this.turningPoint.x, this.turningPoint.y, 1, 1); //Dibujo el punto de giro (central al objeto)

    }
}