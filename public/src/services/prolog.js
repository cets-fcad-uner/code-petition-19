import HttpService from './http.js';

export default class PrologService extends HttpService {
    constructor () {
        super('/pacman/query');
    }

    plQuery(query) {
        return super.post({ query })
    }
}