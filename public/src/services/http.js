export default class HttpService {
    constructor (endpoint) {
        this.apiUrl = '/api' + endpoint;
    }

    /**
     * 
     * @param {Object} options 
     * @param {['POST' | 'GET']} options.method
     * @param {string} options.params
     * @param {Object} options.data
     */
    callApi(options) {
        const url = this.getUrl(options.params || '');
        const headers = this.getHeaders();
        const fetchOptions = {
            url, headers, body: JSON.stringify(options.data || {})
        }
        Object.assign(options, fetchOptions);

        return fetch(url, options)
            .then(data => data.json())
            .then(res => {
                console.log(res);
                return res;
            })
            .catch(console.error)
    }

    post(data = {}) {
        return this.callApi({ method: 'POST', data });
    }

    get(params = '') {
        return this.callApi({ method: 'GET', params });
    }

    getUrl(params = '') {
        return this.apiUrl + (params ? '?' + params : '');
    }

    getHeaders() {
        return { 'Accept': 'application/json', 'Content-Type': 'application/json; charset=UTF-8' };
    }

}